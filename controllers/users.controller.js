var db = require('../db');
var shortid = require('shortid');

module.exports.index = function(req, res, next) {
    var users = db.get('users').value();
    res.render('users/users', {
        title: 'Users',
        msg: 'List user!',
        users: users
    })
};

module.exports.getInfo = function(req, res, next) {
    var id = req.params.id;
    var user = db.get('users').find({id: id}).value();
    res.render('users/view', {
        title: 'View user',
        users: user
    });
}

module.exports.search = function(req, res, next) {
    var q = req.query.q;
    var matchusers = db.get('users').value().filter(function(item) {
        return item.name.toLowerCase().indexOf(q.toLowerCase()) !== -1;
    });
    res.render('users/users', {
        title: 'Kết quả',
        users: matchusers
    })
}

module.exports.create = function(req, res, next) {
    res.render('users/create', {
        title: 'Create new user'
    })
}

module.exports.postCreate = function(req, res, next) {
    var id = shortid.generate();
    var name = req.body.name;
    var phone = req.body.phone;
    db.get('users').push({id: id, name: name, phone: phone}).write();
    res.redirect('/users');
}