var md5 = require('md5');
var db = require('../db');

module.exports.auth = function(req, res, next) {
    res.render('auth/login', {
        title: "Login"
    })
}

module.exports.postLogin = function(req, res, next) {
    var email = req.body.email;
    var password = req.body.password;
    var hashPassword = md5(password);

    var foundUser = db.get('users').find({email: email}).value();

    if(!foundUser) {
        res.render('auth/login', {
            title: 'Login',
            values: req.body,
            error: [
                'The user does not exist'
            ]
        })
    }
    if(hashPassword !== foundUser.password) {
        res.render('auth/login', {
            title: 'Login',
            values: req.body,
            error: [
                'Wrong password'
            ]
        })
    }
    res.cookie('userId', foundUser.id, {
        signed: true
    })
    res.redirect('/users');
}