var express = require('express');
var router = express.Router();

//controller
var authController = require('../controllers/auth.controller');

router.get('/login', authController.auth)

router.post('/login', authController.postLogin)

module.exports = router;