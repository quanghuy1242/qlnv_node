var express = require('express');
var router = express.Router();

//controllers
var userController = require('../controllers/users.controller');
var validate = require('../middlewares/createValidate.middleware');

router.get('/', userController.index);

router.get('/search', userController.search);

router.get('/create', userController.create);

router.get('/:id', userController.getInfo);

router.post('/create', validate.postCreate, userController.postCreate);

module.exports = router;