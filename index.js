var express = require('express');

var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser')

var app = express();
var port = process.env.PORT || 3000;

app.set('view engine', 'pug');
app.set('views', './views');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser('BDJBjbjdksbjdb5651515'));

//Routes
var usersRoute = require('./routes/users.route')
var indexRoute = require('./routes/index.route')
var authRoute = require('./routes/auth.route')

//middleware
var requireAuth = require('./middlewares/requireAuth.controller')

app.use('/', indexRoute)
app.use('/users',requireAuth.requireAuth, usersRoute)
app.use('/auth', authRoute)

app.listen(port, function() {
	console.log(`Port: ${port}`)
})